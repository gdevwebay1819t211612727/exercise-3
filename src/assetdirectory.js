export let AssetDirectory = {
	"load": [
		"assets/fonts/Air Americana.ttf",
		"assets/fonts/AirAmericana.eot",
		"assets/fonts/AirAmericana.svg",
		"assets/fonts/AirAmericana.woff",
		"assets/fonts/AirAmericana.woff2",
		"assets/images/Arrow.png",
		"assets/images/Asteroid.png",
		"assets/images/Banan.png",
		"assets/images/Blinky.png",
		"assets/images/Coin.png",
		"assets/images/PacMan.png",
		"assets/images/SmallPac.png",
		"assets/images/Smush.png"
	],
	"audio": []
};