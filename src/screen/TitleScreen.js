import { Container, Text,TEXT_GRADIENT,ticker,utils,Sprite,Graphics } from "pixi.js";
import KeyCodes from "../KeyCodes";
import KeyValues from "../KeyValues";

class TitleScreen extends Container{
    constructor(){
        super();
        this.interactive = true;
        this.elapsedTime = 0;
        this.ticker = ticker.shared;
        this.ticker.add(this.update,this);
        this.time = 0;
        this.second=0;

        var cache = utils.TextureCache;
        this.image = new Sprite(cache["assets/images/SmallPac.png"]);
        this.image.y = 300;
        this.image2 = new Sprite(cache["assets/images/Asteroid.png"]);
        this.image3 = new Sprite(cache["assets/images/Coin.png"]);
        this.image4 = new Sprite(cache["assets/images/Coin.png"]);
        this.image4.x = 200;
        this.image4.y = 50;
        this.image4.alpha = 0.1;
        this.image6 = new Sprite(cache["assets/images/SmallPac.png"]);
        this.addChild(this.image6);
        this.addChild(this.image);
        this.addChild(this.image2);
        this.addChild(this.image3);
        this.addChild(this.image4);
        this.addChild(this.image6);

        
        //window.addEventListener("keydown",this.onKeyDown.bind(this));
        //window.addEventListener("keyup",this.onKeyUp.bind(this));
        //window.addEventListener("keypress",this.onKeyPress.bind(this));

        window.addEventListener("mousedown",this.mouseDown.bind(this));
        window.addEventListener("mousemove",this.mouseMove.bind(this));

        
        this.ticker.add(this.updateObjs,this);
    }

    mouseDown(z){
        this.image3.x = z.x;
        this.image3.y = z.y;
    }

    mouseMove(p){
        this.image2.x = p.x;
        this.image2.y = p.y;
    }
    
    update(st)
    {
        console.log("Update");
        this.elapsedTime += this.ticker.elapsedMS;
    }

    moveObj(obj)
    {
        obj.x += (this.deltaTime()*100);
        if(obj.x>1000)
        {
            obj.x=0;
        }
    }
    rotateObj(obj)
    {
        obj.rotation += (this.deltaTime()*0.28);
    }
    updateText(obj)
    {
        //time += this.ticker.elapsedMS;
        //obj.Text = "Time = "+
    }
    moveXObj(obj)
    {
        obj.x += 200;
        if(obj.x>1000)
        {
            obj.x=0;
        }
    }
    moveOpacityObj(obj)
    {
        obj.y += (this.deltaTime()*100);
        obj.alpha += (this.deltaTime()*1.);
        if(obj.alpha >= 1.)
        {
            obj.alpha = 0.1;
        }
        if(obj.y>500)
        {
            obj.y=50; 
        }
    }
    updateObjs()
    {
        //this.updateText(this.displayElap);
        this.time += this.deltaTime();
        this.second += this.deltaTime();
        console.log("Delta = "+this.deltaTime());
        this.moveObj(this.image);
        this.rotateObj(this.image);
        this.moveOpacityObj(this.image4);
        if(this.second>=1.)
        {
        this.moveXObj(this.image6);
        var cache = utils.TextureCache;
        this.image6 = new Sprite(cache["assets/images/Coin.png"]);
        this.second = 0;
        }
    }
    deltaTime()
    {
        return this.ticker.elapsedMS/1000;
    }

}
export default TitleScreen;