const path = require('path');
const configData = require('../config.json');

function BuildConfig() {
    console.log("Initializing Build Config...");
    this._init();
}

BuildConfig.prototype._init = function () {
    this._title;
    this._gameWidth;
    this._gameHeight;

    this._externalMinified;
    this._externalLocation;
    this._externals = [];

    this._title = configData.BUILD.GAME_TITLE;
    this._gameWidth = configData.BUILD.WIDTH;
    this._gameHeight = configData.BUILD.HEIGHT;
    this._externalMinified = configData.BUILD.MINIFIED_EXTERNAL_JS;

    this._externals = configData.BUILD.EXTERNAL_JS;

    this.initExternals();
}

BuildConfig.prototype.initExternals = function () {
    var fileExtension;

    if (this._externalMinified) {
        this._externalLocation = configData.BUILD.MINIFIED_EXTERNAL_JS_PATH;

        fileExtension = ".min.js";
    } else {
        this._externalLocation = configData.BUILD.UNMINIFIED_EXTERNAL_JS_PATH;

        fileExtension = ".js";
    }

    for (var i = 0; i < this._externals.length; i++) {
        this._externals[i] = this._externals[i] + fileExtension;
    }
}

BuildConfig.prototype.getExternals = function () {
    return this._externals;
}

BuildConfig.prototype.getExternalOutputPaths = function (output) {
    var externalsWithOutputPath = [];

    for (var i = 0; i < this._externals.length; i++) {
        externalsWithOutputPath.push(output + this._externals[i]);
    }

    return externalsWithOutputPath;
}

BuildConfig.prototype.getExternalLocation = function () {
    return this._externalLocation;
}

BuildConfig.prototype.getGameTitle = function () {
    return this._title;
}

var buildConfig = new BuildConfig();

module.exports = buildConfig;