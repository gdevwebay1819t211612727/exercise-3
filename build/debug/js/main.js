/******/ (function(modules) { // webpackBootstrap
/******/ 	// install a JSONP callback for chunk loading
/******/ 	function webpackJsonpCallback(data) {
/******/ 		var chunkIds = data[0];
/******/ 		var moreModules = data[1];
/******/ 		var executeModules = data[2];
/******/
/******/ 		// add "moreModules" to the modules object,
/******/ 		// then flag all "chunkIds" as loaded and fire callback
/******/ 		var moduleId, chunkId, i = 0, resolves = [];
/******/ 		for(;i < chunkIds.length; i++) {
/******/ 			chunkId = chunkIds[i];
/******/ 			if(installedChunks[chunkId]) {
/******/ 				resolves.push(installedChunks[chunkId][0]);
/******/ 			}
/******/ 			installedChunks[chunkId] = 0;
/******/ 		}
/******/ 		for(moduleId in moreModules) {
/******/ 			if(Object.prototype.hasOwnProperty.call(moreModules, moduleId)) {
/******/ 				modules[moduleId] = moreModules[moduleId];
/******/ 			}
/******/ 		}
/******/ 		if(parentJsonpFunction) parentJsonpFunction(data);
/******/
/******/ 		while(resolves.length) {
/******/ 			resolves.shift()();
/******/ 		}
/******/
/******/ 		// add entry modules from loaded chunk to deferred list
/******/ 		deferredModules.push.apply(deferredModules, executeModules || []);
/******/
/******/ 		// run deferred modules when all chunks ready
/******/ 		return checkDeferredModules();
/******/ 	};
/******/ 	function checkDeferredModules() {
/******/ 		var result;
/******/ 		for(var i = 0; i < deferredModules.length; i++) {
/******/ 			var deferredModule = deferredModules[i];
/******/ 			var fulfilled = true;
/******/ 			for(var j = 1; j < deferredModule.length; j++) {
/******/ 				var depId = deferredModule[j];
/******/ 				if(installedChunks[depId] !== 0) fulfilled = false;
/******/ 			}
/******/ 			if(fulfilled) {
/******/ 				deferredModules.splice(i--, 1);
/******/ 				result = __webpack_require__(__webpack_require__.s = deferredModule[0]);
/******/ 			}
/******/ 		}
/******/ 		return result;
/******/ 	}
/******/
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// object to store loaded and loading chunks
/******/ 	// undefined = chunk not loaded, null = chunk preloaded/prefetched
/******/ 	// Promise = chunk loading, 0 = chunk loaded
/******/ 	var installedChunks = {
/******/ 		"main": 0
/******/ 	};
/******/
/******/ 	var deferredModules = [];
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	var jsonpArray = window["webpackJsonp"] = window["webpackJsonp"] || [];
/******/ 	var oldJsonpFunction = jsonpArray.push.bind(jsonpArray);
/******/ 	jsonpArray.push = webpackJsonpCallback;
/******/ 	jsonpArray = jsonpArray.slice();
/******/ 	for(var i = 0; i < jsonpArray.length; i++) webpackJsonpCallback(jsonpArray[i]);
/******/ 	var parentJsonpFunction = oldJsonpFunction;
/******/
/******/
/******/ 	// add entry module to deferred list
/******/ 	deferredModules.push(["./src/index.js","vendor"]);
/******/ 	// run deferred modules when ready
/******/ 	return checkDeferredModules();
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/KeyCodes.js":
/*!*************************!*\
  !*** ./src/KeyCodes.js ***!
  \*************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nObject.defineProperty(exports, \"__esModule\", {\n    value: true\n});\nvar KeyCodes = {\n    KeySpace: 32,\n    KeyA: 65,\n    KeyB: 66,\n    KeyC: 67,\n    KeyD: 68,\n    KeyE: 69,\n    KeyF: 70,\n    KeyG: 71,\n    KeyH: 72,\n    KeyI: 73,\n    KeyJ: 74,\n    KeyK: 75,\n    KeyL: 76,\n    KeyM: 77,\n    KeyN: 78,\n    KeyO: 79,\n    KeyP: 80,\n    KeyQ: 81,\n    KeyR: 82,\n    KeyS: 83,\n    KeyT: 84,\n    KeyU: 85,\n    KeyV: 86,\n    KeyW: 87,\n    KeyX: 88,\n    KeyY: 89,\n    KeyZ: 90,\n    Key0: 48,\n    Key1: 49,\n    Key2: 50,\n    Key3: 51,\n    Key4: 52,\n    Key5: 53,\n    Key6: 54,\n    Key7: 55,\n    Key8: 56,\n    Key9: 57,\n    KeyNumPad0: 96,\n    KeyNumPad1: 97,\n    KeyNumPad2: 98,\n    KeyNumPad3: 99,\n    KeyNumPad4: 100,\n    KeyNumPad5: 101,\n    KeyNumPad6: 102,\n    KeyNumPad7: 103,\n    KeyNumPad8: 104,\n    KeyNumPad9: 105,\n    KeyArrowUp: 38,\n    KeyArrowDown: 40,\n    KeyArrowLeft: 37,\n    KeyArrowRight: 39\n};\n\nexports.default = KeyCodes;//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvS2V5Q29kZXMuanMuanMiLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vc3JjL0tleUNvZGVzLmpzPzE3ZDMiXSwic291cmNlc0NvbnRlbnQiOlsidmFyIEtleUNvZGVzID0ge1xyXG4gICAgS2V5U3BhY2U6IDMyLFxyXG4gICAgS2V5QSA6IDY1LFxyXG4gICAgS2V5QiA6IDY2LFxyXG4gICAgS2V5QyA6IDY3LFxyXG4gICAgS2V5RCA6IDY4LFxyXG4gICAgS2V5RSA6IDY5LFxyXG4gICAgS2V5RiA6IDcwLFxyXG4gICAgS2V5RyA6IDcxLFxyXG4gICAgS2V5SCA6IDcyLFxyXG4gICAgS2V5SSA6IDczLFxyXG4gICAgS2V5SiA6IDc0LFxyXG4gICAgS2V5SyA6IDc1LFxyXG4gICAgS2V5TCA6IDc2LFxyXG4gICAgS2V5TSA6IDc3LFxyXG4gICAgS2V5TiA6IDc4LFxyXG4gICAgS2V5TyA6IDc5LFxyXG4gICAgS2V5UCA6IDgwLFxyXG4gICAgS2V5USA6IDgxLFxyXG4gICAgS2V5UiA6IDgyLFxyXG4gICAgS2V5UyA6IDgzLFxyXG4gICAgS2V5VCA6IDg0LFxyXG4gICAgS2V5VSA6IDg1LFxyXG4gICAgS2V5ViA6IDg2LFxyXG4gICAgS2V5VyA6IDg3LFxyXG4gICAgS2V5WCA6IDg4LFxyXG4gICAgS2V5WSA6IDg5LFxyXG4gICAgS2V5WiA6IDkwLFxyXG4gICAgS2V5MCA6IDQ4LFxyXG4gICAgS2V5MSA6IDQ5LFxyXG4gICAgS2V5MiA6IDUwLFxyXG4gICAgS2V5MyA6IDUxLFxyXG4gICAgS2V5NCA6IDUyLFxyXG4gICAgS2V5NSA6IDUzLFxyXG4gICAgS2V5NiA6IDU0LFxyXG4gICAgS2V5NyA6IDU1LFxyXG4gICAgS2V5OCA6IDU2LFxyXG4gICAgS2V5OSA6IDU3LFxyXG4gICAgS2V5TnVtUGFkMCA6IDk2LFxyXG4gICAgS2V5TnVtUGFkMSA6IDk3LFxyXG4gICAgS2V5TnVtUGFkMiA6IDk4LFxyXG4gICAgS2V5TnVtUGFkMyA6IDk5LFxyXG4gICAgS2V5TnVtUGFkNCA6IDEwMCxcclxuICAgIEtleU51bVBhZDUgOiAxMDEsXHJcbiAgICBLZXlOdW1QYWQ2IDogMTAyLFxyXG4gICAgS2V5TnVtUGFkNyA6IDEwMyxcclxuICAgIEtleU51bVBhZDggOiAxMDQsXHJcbiAgICBLZXlOdW1QYWQ5IDogMTA1LFxyXG4gICAgS2V5QXJyb3dVcCA6IDM4LFxyXG4gICAgS2V5QXJyb3dEb3duIDogNDAsXHJcbiAgICBLZXlBcnJvd0xlZnQgOiAzNyxcclxuICAgIEtleUFycm93UmlnaHQgOiAzOVxyXG59XHJcblxyXG5leHBvcnQgZGVmYXVsdCBLZXlDb2RlczsiXSwibWFwcGluZ3MiOiI7Ozs7O0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFuREE7QUFDQTtBQXFEQSIsInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///./src/KeyCodes.js\n");

/***/ }),

/***/ "./src/KeyValues.js":
/*!**************************!*\
  !*** ./src/KeyValues.js ***!
  \**************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nObject.defineProperty(exports, \"__esModule\", {\n    value: true\n});\nvar KeyValues = {\n    32: \" \",\n    65: \"A\",\n    66: \"B\",\n    67: \"C\",\n    68: \"D\",\n    69: \"E\",\n    70: \"F\",\n    71: \"G\",\n    72: \"H\",\n    73: \"I\",\n    74: \"J\",\n    75: \"K\",\n    76: \"L\",\n    77: \"M\",\n    78: \"N\",\n    79: \"O\",\n    80: \"P\",\n    81: \"Q\",\n    82: \"R\",\n    83: \"S\",\n    84: \"T\",\n    85: \"U\",\n    86: \"V\",\n    87: \"W\",\n    88: \"X\",\n    89: \"Y\",\n    90: \"Z\",\n    48: \"0\",\n    49: \"1\",\n    50: \"2\",\n    51: \"3\",\n    52: \"4\",\n    53: \"5\",\n    54: \"6\",\n    55: \"7\",\n    56: \"8\",\n    57: \"9\",\n    96: \"0\",\n    97: \"1\",\n    98: \"2\",\n    99: \"3\",\n    100: \"4\",\n    101: \"5\",\n    102: \"6\",\n    103: \"7\",\n    104: \"8\",\n    105: \"9\",\n    37: \"ArrowLeft\",\n    38: \"ArrowUp\",\n    39: \"ArrowRight\",\n    40: \"ArrowDown \"\n\n};\n\nexports.default = KeyValues;//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvS2V5VmFsdWVzLmpzLmpzIiwic291cmNlcyI6WyJ3ZWJwYWNrOi8vL3NyYy9LZXlWYWx1ZXMuanM/NjcyOSJdLCJzb3VyY2VzQ29udGVudCI6WyJ2YXIgS2V5VmFsdWVzID0ge1xyXG4gICAgMzIgOiBcIiBcIixcclxuICAgIDY1IDogXCJBXCIsXHJcbiAgICA2NiA6IFwiQlwiLFxyXG4gICAgNjcgOiBcIkNcIixcclxuICAgIDY4IDogXCJEXCIsXHJcbiAgICA2OSA6IFwiRVwiLFxyXG4gICAgNzAgOiBcIkZcIixcclxuICAgIDcxIDogXCJHXCIsXHJcbiAgICA3MiA6IFwiSFwiLFxyXG4gICAgNzMgOiBcIklcIixcclxuICAgIDc0IDogXCJKXCIsXHJcbiAgICA3NSA6IFwiS1wiLFxyXG4gICAgNzYgOiBcIkxcIixcclxuICAgIDc3IDogXCJNXCIsXHJcbiAgICA3OCA6IFwiTlwiLFxyXG4gICAgNzkgOiBcIk9cIixcclxuICAgIDgwIDogXCJQXCIsXHJcbiAgICA4MSA6IFwiUVwiLFxyXG4gICAgODIgOiBcIlJcIixcclxuICAgIDgzIDogXCJTXCIsXHJcbiAgICA4NCA6IFwiVFwiLFxyXG4gICAgODUgOiBcIlVcIixcclxuICAgIDg2IDogXCJWXCIsXHJcbiAgICA4NyA6IFwiV1wiLFxyXG4gICAgODggOiBcIlhcIixcclxuICAgIDg5IDogXCJZXCIsXHJcbiAgICA5MCA6IFwiWlwiLFxyXG4gICAgNDggOiBcIjBcIixcclxuICAgIDQ5IDogXCIxXCIsXHJcbiAgICA1MCA6IFwiMlwiLFxyXG4gICAgNTEgOiBcIjNcIixcclxuICAgIDUyIDogXCI0XCIsXHJcbiAgICA1MyA6IFwiNVwiLFxyXG4gICAgNTQgOiBcIjZcIixcclxuICAgIDU1IDogXCI3XCIsXHJcbiAgICA1NiA6IFwiOFwiLFxyXG4gICAgNTcgOiBcIjlcIixcclxuICAgIDk2IDogXCIwXCIsXHJcbiAgICA5NyA6IFwiMVwiLFxyXG4gICAgOTggOiBcIjJcIixcclxuICAgIDk5IDogXCIzXCIsXHJcbiAgICAxMDAgOiBcIjRcIixcclxuICAgIDEwMSA6IFwiNVwiLFxyXG4gICAgMTAyIDogXCI2XCIsXHJcbiAgICAxMDMgOiBcIjdcIixcclxuICAgIDEwNCA6IFwiOFwiLFxyXG4gICAgMTA1IDogXCI5XCIsXHJcbiAgICAzNyA6IFwiQXJyb3dMZWZ0XCIsXHJcbiAgICAzOCA6IFwiQXJyb3dVcFwiLFxyXG4gICAgMzkgOiBcIkFycm93UmlnaHRcIixcclxuICAgIDQwIDogXCJBcnJvd0Rvd24gXCIsXHJcblxyXG5cclxuXHJcbn1cclxuXHJcbmV4cG9ydCBkZWZhdWx0IEtleVZhbHVlczsiXSwibWFwcGluZ3MiOiI7Ozs7O0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQXBEQTtBQUNBO0FBd0RBIiwic291cmNlUm9vdCI6IiJ9\n//# sourceURL=webpack-internal:///./src/KeyValues.js\n");

/***/ }),

/***/ "./src/Main.js":
/*!*********************!*\
  !*** ./src/Main.js ***!
  \*********************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nObject.defineProperty(exports, \"__esModule\", {\n    value: true\n});\n\nvar _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if (\"value\" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();\n\nvar _assetdirectory = __webpack_require__(/*! ./assetdirectory */ \"./src/assetdirectory.js\");\n\nvar _pixi = __webpack_require__(/*! pixi.js */ \"./node_modules/pixi.js/lib/index.js\");\n\nvar _TitleScreen = __webpack_require__(/*! ./screen/TitleScreen */ \"./src/screen/TitleScreen.js\");\n\nvar _TitleScreen2 = _interopRequireDefault(_TitleScreen);\n\nvar _ = __webpack_require__(/*! . */ \"./src/index.js\");\n\nfunction _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }\n\nfunction _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError(\"Cannot call a class as a function\"); } }\n\nvar Main = function () {\n    function Main() {\n        _classCallCheck(this, Main);\n    }\n\n    _createClass(Main, null, [{\n        key: \"start\",\n        value: function start() {\n            Main.cjAudioQueue = new createjs.LoadQueue();\n            createjs.Sound.alternateExtensions = [\"ogg\"];\n\n            Main.cjAudioQueue.installPlugin(createjs.Sound);\n            Main.cjAudioQueue.addEventListener(\"complete\", Main.handleAudioComplete.bind(Main));\n\n            if (_assetdirectory.AssetDirectory.audio.length > 0) {\n                //LOAD AUDIO                  \n                var audioFiles = _assetdirectory.AssetDirectory.audio;\n                var audioManifest = [];\n                for (var i = 0; i < audioFiles.length; i++) {\n                    audioManifest.push({\n                        id: audioFiles[i],\n                        src: audioFiles[i]\n                    });\n                }\n                Main.cjAudioQueue.loadManifest(audioManifest);\n            } else {\n                Main.handleAudioComplete();\n            }\n        }\n    }, {\n        key: \"handleAudioComplete\",\n        value: function handleAudioComplete() {\n            if (_assetdirectory.AssetDirectory.load.length > 0) {\n                //LOAD IMAGES         \n                var loader = _pixi.loaders.shared;\n                loader.add(_assetdirectory.AssetDirectory.load);\n                loader.load(Main.handleImageComplete);\n            } else {\n                Main.handleImageComplete();\n            }\n        }\n    }, {\n        key: \"handleImageComplete\",\n        value: function handleImageComplete() {\n            var screen = new _TitleScreen2.default();\n            _.App.stage.addChild(screen);\n        }\n    }]);\n\n    return Main;\n}();\n\nexports.default = Main;//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvTWFpbi5qcy5qcyIsInNvdXJjZXMiOlsid2VicGFjazovLy9zcmMvTWFpbi5qcz8xMjIxIl0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEFzc2V0RGlyZWN0b3J5IH0gZnJvbSBcIi4vYXNzZXRkaXJlY3RvcnlcIjtcclxuaW1wb3J0IHsgbG9hZGVycyB9IGZyb20gXCJwaXhpLmpzXCI7XHJcbmltcG9ydCBUaXRsZVNjcmVlbiBmcm9tIFwiLi9zY3JlZW4vVGl0bGVTY3JlZW5cIjtcclxuaW1wb3J0IHsgQXBwIH0gZnJvbSBcIi5cIjtcclxuXHJcblxyXG5jbGFzcyBNYWluIHtcclxuICAgIHN0YXRpYyBzdGFydCgpe1xyXG4gICAgICAgIE1haW4uY2pBdWRpb1F1ZXVlID0gbmV3IGNyZWF0ZWpzLkxvYWRRdWV1ZSgpO1xyXG4gICAgICAgIGNyZWF0ZWpzLlNvdW5kLmFsdGVybmF0ZUV4dGVuc2lvbnMgPSBbXCJvZ2dcIl07XHJcblxyXG4gICAgICAgIE1haW4uY2pBdWRpb1F1ZXVlLmluc3RhbGxQbHVnaW4oY3JlYXRlanMuU291bmQpO1xyXG4gICAgICAgIE1haW4uY2pBdWRpb1F1ZXVlLmFkZEV2ZW50TGlzdGVuZXIoXCJjb21wbGV0ZVwiLCBNYWluLmhhbmRsZUF1ZGlvQ29tcGxldGUuYmluZChNYWluKSk7XHJcblxyXG4gICAgICAgIGlmKEFzc2V0RGlyZWN0b3J5LmF1ZGlvLmxlbmd0aCA+IDApe1xyXG4gICAgICAgICAgICAvL0xPQUQgQVVESU8gICAgICAgICAgICAgICAgICBcclxuICAgICAgICAgICAgbGV0IGF1ZGlvRmlsZXMgPSBBc3NldERpcmVjdG9yeS5hdWRpbztcclxuICAgICAgICAgICAgbGV0IGF1ZGlvTWFuaWZlc3QgPSBbXTtcclxuICAgICAgICAgICAgZm9yKGxldCBpID0gMDsgaSA8IGF1ZGlvRmlsZXMubGVuZ3RoOyBpKyspe1xyXG4gICAgICAgICAgICAgICAgYXVkaW9NYW5pZmVzdC5wdXNoKHtcclxuICAgICAgICAgICAgICAgICAgICBpZDogYXVkaW9GaWxlc1tpXSxcclxuICAgICAgICAgICAgICAgICAgICBzcmM6IGF1ZGlvRmlsZXNbaV1cclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIE1haW4uY2pBdWRpb1F1ZXVlLmxvYWRNYW5pZmVzdChhdWRpb01hbmlmZXN0KTtcclxuICAgICAgICB9XHJcbiAgICAgICAgZWxzZXtcclxuICAgICAgICAgICAgTWFpbi5oYW5kbGVBdWRpb0NvbXBsZXRlKCk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgIH1cclxuXHJcbiAgICBzdGF0aWMgaGFuZGxlQXVkaW9Db21wbGV0ZSgpe1xyXG4gICAgICAgIGlmKEFzc2V0RGlyZWN0b3J5LmxvYWQubGVuZ3RoID4gMCl7XHJcbiAgICAgICAgICAgIC8vTE9BRCBJTUFHRVMgICAgICAgICBcclxuICAgICAgICAgICAgbGV0IGxvYWRlciA9IGxvYWRlcnMuc2hhcmVkO1xyXG4gICAgICAgICAgICBsb2FkZXIuYWRkKEFzc2V0RGlyZWN0b3J5LmxvYWQpO1xyXG4gICAgICAgICAgICBsb2FkZXIubG9hZChNYWluLmhhbmRsZUltYWdlQ29tcGxldGUpO1xyXG4gICAgICAgIH1cclxuICAgICAgICBlbHNlIHtcclxuICAgICAgICAgICAgTWFpbi5oYW5kbGVJbWFnZUNvbXBsZXRlKCk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHN0YXRpYyBoYW5kbGVJbWFnZUNvbXBsZXRlKCl7XHJcbiAgICAgICAgbGV0IHNjcmVlbiA9IG5ldyBUaXRsZVNjcmVlbigpO1xyXG4gICAgICAgIEFwcC5zdGFnZS5hZGRDaGlsZChzY3JlZW4pO1xyXG4gICAgfVxyXG59XHJcblxyXG5leHBvcnQgZGVmYXVsdCBNYWluOyJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7OztBQUFBO0FBQ0E7Ozs7O0FBRUE7Ozs7Ozs7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFFQTs7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7QUFHQSIsInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///./src/Main.js\n");

/***/ }),

/***/ "./src/assetdirectory.js":
/*!*******************************!*\
  !*** ./src/assetdirectory.js ***!
  \*******************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nObject.defineProperty(exports, \"__esModule\", {\n\tvalue: true\n});\nvar AssetDirectory = exports.AssetDirectory = {\n\t\"load\": [\"assets/fonts/Air Americana.ttf\", \"assets/fonts/AirAmericana.eot\", \"assets/fonts/AirAmericana.svg\", \"assets/fonts/AirAmericana.woff\", \"assets/fonts/AirAmericana.woff2\", \"assets/images/Arrow.png\", \"assets/images/Asteroid.png\", \"assets/images/Banan.png\", \"assets/images/Blinky.png\", \"assets/images/Coin.png\", \"assets/images/PacMan.png\", \"assets/images/SmallPac.png\", \"assets/images/Smush.png\"],\n\t\"audio\": []\n};//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXNzZXRkaXJlY3RvcnkuanMuanMiLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vc3JjL2Fzc2V0ZGlyZWN0b3J5LmpzPzZmZDEiXSwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0IGxldCBBc3NldERpcmVjdG9yeSA9IHtcblx0XCJsb2FkXCI6IFtcblx0XHRcImFzc2V0cy9mb250cy9BaXIgQW1lcmljYW5hLnR0ZlwiLFxuXHRcdFwiYXNzZXRzL2ZvbnRzL0FpckFtZXJpY2FuYS5lb3RcIixcblx0XHRcImFzc2V0cy9mb250cy9BaXJBbWVyaWNhbmEuc3ZnXCIsXG5cdFx0XCJhc3NldHMvZm9udHMvQWlyQW1lcmljYW5hLndvZmZcIixcblx0XHRcImFzc2V0cy9mb250cy9BaXJBbWVyaWNhbmEud29mZjJcIixcblx0XHRcImFzc2V0cy9pbWFnZXMvQXJyb3cucG5nXCIsXG5cdFx0XCJhc3NldHMvaW1hZ2VzL0FzdGVyb2lkLnBuZ1wiLFxuXHRcdFwiYXNzZXRzL2ltYWdlcy9CYW5hbi5wbmdcIixcblx0XHRcImFzc2V0cy9pbWFnZXMvQmxpbmt5LnBuZ1wiLFxuXHRcdFwiYXNzZXRzL2ltYWdlcy9Db2luLnBuZ1wiLFxuXHRcdFwiYXNzZXRzL2ltYWdlcy9QYWNNYW4ucG5nXCIsXG5cdFx0XCJhc3NldHMvaW1hZ2VzL1NtYWxsUGFjLnBuZ1wiLFxuXHRcdFwiYXNzZXRzL2ltYWdlcy9TbXVzaC5wbmdcIlxuXHRdLFxuXHRcImF1ZGlvXCI6IFtdXG59OyJdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQTtBQUNBO0FBZUE7QUFoQkEiLCJzb3VyY2VSb290IjoiIn0=\n//# sourceURL=webpack-internal:///./src/assetdirectory.js\n");

/***/ }),

/***/ "./src/index.js":
/*!**********************!*\
  !*** ./src/index.js ***!
  \**********************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nObject.defineProperty(exports, \"__esModule\", {\n    value: true\n});\nexports.App = undefined;\n\nvar _assetdirectory = __webpack_require__(/*! ./assetdirectory.js */ \"./src/assetdirectory.js\");\n\nvar _pixi = __webpack_require__(/*! pixi.js */ \"./node_modules/pixi.js/lib/index.js\");\n\nvar _Main = __webpack_require__(/*! ./Main.js */ \"./src/Main.js\");\n\nvar _Main2 = _interopRequireDefault(_Main);\n\nfunction _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }\n\nvar Config = __webpack_require__(/*! Config */ \"Config\");\n\nvar canvas = document.getElementById('game-canvas');\nvar pixiapp = new _pixi.Application({\n    view: canvas,\n    width: Config.BUILD.WIDTH,\n    height: Config.BUILD.HEIGHT\n});\n\ndocument.body.style.margin = \"0px\";\ndocument.body.style.overflow = \"hidden\";\n\n/*****************************************\r\n ************* ENTRY POINT ***************\r\n *****************************************/\nfunction ready(fn) {\n    if (document.readyState != 'loading') {\n        fn();\n    } else {\n        document.addEventListener('DOMContentLoaded', fn);\n    }\n}\n\nready(function () {\n    _Main2.default.start();\n});\n\nexports.App = pixiapp;//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvaW5kZXguanMuanMiLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vc3JjL2luZGV4LmpzPzEyZDUiXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQXNzZXREaXJlY3RvcnkgfSBmcm9tICcuL2Fzc2V0ZGlyZWN0b3J5LmpzJztcclxuaW1wb3J0IHsgQXBwbGljYXRpb24gfSBmcm9tICdwaXhpLmpzJztcclxuaW1wb3J0IE1haW4gZnJvbSAnLi9NYWluLmpzJztcclxuXHJcbnZhciBDb25maWcgPSByZXF1aXJlKCdDb25maWcnKTtcclxuXHJcblxyXG5sZXQgY2FudmFzID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoJ2dhbWUtY2FudmFzJyk7XHJcbmxldCBwaXhpYXBwID0gbmV3IEFwcGxpY2F0aW9uKHtcclxuICAgIHZpZXc6IGNhbnZhcyxcclxuICAgIHdpZHRoOiBDb25maWcuQlVJTEQuV0lEVEgsXHJcbiAgICBoZWlnaHQ6IENvbmZpZy5CVUlMRC5IRUlHSFRcclxufSlcclxuXHJcblxyXG5kb2N1bWVudC5ib2R5LnN0eWxlLm1hcmdpbiA9IFwiMHB4XCI7XHJcbmRvY3VtZW50LmJvZHkuc3R5bGUub3ZlcmZsb3cgPSBcImhpZGRlblwiO1xyXG5cclxuLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqXHJcbiAqKioqKioqKioqKioqIEVOVFJZIFBPSU5UICoqKioqKioqKioqKioqKlxyXG4gKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiovXHJcbmZ1bmN0aW9uIHJlYWR5KGZuKSB7XHJcbiAgICBpZiAoZG9jdW1lbnQucmVhZHlTdGF0ZSAhPSAnbG9hZGluZycpIHtcclxuICAgICAgICBmbigpO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgICBkb2N1bWVudC5hZGRFdmVudExpc3RlbmVyKCdET01Db250ZW50TG9hZGVkJywgZm4pO1xyXG4gICAgfVxyXG59XHJcblxyXG5yZWFkeShmdW5jdGlvbiAoKSB7XHJcbiAgICBNYWluLnN0YXJ0KCk7XHJcbn0pO1xyXG5cclxuZXhwb3J0IHtwaXhpYXBwIGFzIEFwcH07Il0sIm1hcHBpbmdzIjoiOzs7Ozs7O0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBOzs7OztBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUFDQTtBQU1BO0FBQ0E7QUFDQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///./src/index.js\n");

/***/ }),

/***/ "./src/screen/TitleScreen.js":
/*!***********************************!*\
  !*** ./src/screen/TitleScreen.js ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nObject.defineProperty(exports, \"__esModule\", {\n    value: true\n});\n\nvar _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if (\"value\" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();\n\nvar _pixi = __webpack_require__(/*! pixi.js */ \"./node_modules/pixi.js/lib/index.js\");\n\nvar _KeyCodes = __webpack_require__(/*! ../KeyCodes */ \"./src/KeyCodes.js\");\n\nvar _KeyCodes2 = _interopRequireDefault(_KeyCodes);\n\nvar _KeyValues = __webpack_require__(/*! ../KeyValues */ \"./src/KeyValues.js\");\n\nvar _KeyValues2 = _interopRequireDefault(_KeyValues);\n\nfunction _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }\n\nfunction _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError(\"Cannot call a class as a function\"); } }\n\nfunction _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError(\"this hasn't been initialised - super() hasn't been called\"); } return call && (typeof call === \"object\" || typeof call === \"function\") ? call : self; }\n\nfunction _inherits(subClass, superClass) { if (typeof superClass !== \"function\" && superClass !== null) { throw new TypeError(\"Super expression must either be null or a function, not \" + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }\n\nvar TitleScreen = function (_Container) {\n    _inherits(TitleScreen, _Container);\n\n    function TitleScreen() {\n        _classCallCheck(this, TitleScreen);\n\n        var _this = _possibleConstructorReturn(this, (TitleScreen.__proto__ || Object.getPrototypeOf(TitleScreen)).call(this));\n\n        _this.interactive = true;\n        _this.elapsedTime = 0;\n        _this.ticker = _pixi.ticker.shared;\n        _this.ticker.add(_this.update, _this);\n        _this.time = 0;\n        _this.second = 0;\n\n        var cache = _pixi.utils.TextureCache;\n        _this.image = new _pixi.Sprite(cache[\"assets/images/SmallPac.png\"]);\n        _this.image.y = 300;\n        _this.image2 = new _pixi.Sprite(cache[\"assets/images/Asteroid.png\"]);\n        _this.image3 = new _pixi.Sprite(cache[\"assets/images/Coin.png\"]);\n        _this.image4 = new _pixi.Sprite(cache[\"assets/images/Coin.png\"]);\n        _this.image4.x = 200;\n        _this.image4.y = 50;\n        _this.image4.alpha = 0.1;\n        _this.image6 = new _pixi.Sprite(cache[\"assets/images/SmallPac.png\"]);\n        _this.addChild(_this.image6);\n        _this.addChild(_this.image);\n        _this.addChild(_this.image2);\n        _this.addChild(_this.image3);\n        _this.addChild(_this.image4);\n        _this.addChild(_this.image6);\n\n        //window.addEventListener(\"keydown\",this.onKeyDown.bind(this));\n        //window.addEventListener(\"keyup\",this.onKeyUp.bind(this));\n        //window.addEventListener(\"keypress\",this.onKeyPress.bind(this));\n\n        window.addEventListener(\"mousedown\", _this.mouseDown.bind(_this));\n        window.addEventListener(\"mousemove\", _this.mouseMove.bind(_this));\n\n        _this.ticker.add(_this.updateObjs, _this);\n        return _this;\n    }\n\n    _createClass(TitleScreen, [{\n        key: \"mouseDown\",\n        value: function mouseDown(z) {\n            this.image3.x = z.x;\n            this.image3.y = z.y;\n        }\n    }, {\n        key: \"mouseMove\",\n        value: function mouseMove(p) {\n            this.image2.x = p.x;\n            this.image2.y = p.y;\n        }\n    }, {\n        key: \"update\",\n        value: function update(st) {\n            console.log(\"Update\");\n            this.elapsedTime += this.ticker.elapsedMS;\n        }\n    }, {\n        key: \"moveObj\",\n        value: function moveObj(obj) {\n            obj.x += this.deltaTime() * 100;\n            if (obj.x > 1000) {\n                obj.x = 0;\n            }\n        }\n    }, {\n        key: \"rotateObj\",\n        value: function rotateObj(obj) {\n            obj.rotation += this.deltaTime() * 0.28;\n        }\n    }, {\n        key: \"updateText\",\n        value: function updateText(obj) {\n            //time += this.ticker.elapsedMS;\n            //obj.Text = \"Time = \"+\n        }\n    }, {\n        key: \"moveXObj\",\n        value: function moveXObj(obj) {\n            obj.x += 200;\n            if (obj.x > 1000) {\n                obj.x = 0;\n            }\n        }\n    }, {\n        key: \"moveOpacityObj\",\n        value: function moveOpacityObj(obj) {\n            obj.y += this.deltaTime() * 100;\n            obj.alpha += this.deltaTime() * 1.;\n            if (obj.alpha >= 1.) {\n                obj.alpha = 0.1;\n            }\n            if (obj.y > 500) {\n                obj.y = 50;\n            }\n        }\n    }, {\n        key: \"updateObjs\",\n        value: function updateObjs() {\n            //this.updateText(this.displayElap);\n            this.time += this.deltaTime();\n            this.second += this.deltaTime();\n            console.log(\"Delta = \" + this.deltaTime());\n            this.moveObj(this.image);\n            this.rotateObj(this.image);\n            this.moveOpacityObj(this.image4);\n            if (this.second >= 1.) {\n                this.moveXObj(this.image6);\n                var cache = _pixi.utils.TextureCache;\n                this.image6 = new _pixi.Sprite(cache[\"assets/images/Coin.png\"]);\n                this.second = 0;\n            }\n        }\n    }, {\n        key: \"deltaTime\",\n        value: function deltaTime() {\n            return this.ticker.elapsedMS / 1000;\n        }\n    }]);\n\n    return TitleScreen;\n}(_pixi.Container);\n\nexports.default = TitleScreen;//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvc2NyZWVuL1RpdGxlU2NyZWVuLmpzLmpzIiwic291cmNlcyI6WyJ3ZWJwYWNrOi8vL3NyYy9zY3JlZW4vVGl0bGVTY3JlZW4uanM/YTM3MiJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb250YWluZXIsIFRleHQsVEVYVF9HUkFESUVOVCx0aWNrZXIsdXRpbHMsU3ByaXRlLEdyYXBoaWNzIH0gZnJvbSBcInBpeGkuanNcIjtcclxuaW1wb3J0IEtleUNvZGVzIGZyb20gXCIuLi9LZXlDb2Rlc1wiO1xyXG5pbXBvcnQgS2V5VmFsdWVzIGZyb20gXCIuLi9LZXlWYWx1ZXNcIjtcclxuXHJcbmNsYXNzIFRpdGxlU2NyZWVuIGV4dGVuZHMgQ29udGFpbmVye1xyXG4gICAgY29uc3RydWN0b3IoKXtcclxuICAgICAgICBzdXBlcigpO1xyXG4gICAgICAgIHRoaXMuaW50ZXJhY3RpdmUgPSB0cnVlO1xyXG4gICAgICAgIHRoaXMuZWxhcHNlZFRpbWUgPSAwO1xyXG4gICAgICAgIHRoaXMudGlja2VyID0gdGlja2VyLnNoYXJlZDtcclxuICAgICAgICB0aGlzLnRpY2tlci5hZGQodGhpcy51cGRhdGUsdGhpcyk7XHJcbiAgICAgICAgdGhpcy50aW1lID0gMDtcclxuICAgICAgICB0aGlzLnNlY29uZD0wO1xyXG5cclxuICAgICAgICB2YXIgY2FjaGUgPSB1dGlscy5UZXh0dXJlQ2FjaGU7XHJcbiAgICAgICAgdGhpcy5pbWFnZSA9IG5ldyBTcHJpdGUoY2FjaGVbXCJhc3NldHMvaW1hZ2VzL1NtYWxsUGFjLnBuZ1wiXSk7XHJcbiAgICAgICAgdGhpcy5pbWFnZS55ID0gMzAwO1xyXG4gICAgICAgIHRoaXMuaW1hZ2UyID0gbmV3IFNwcml0ZShjYWNoZVtcImFzc2V0cy9pbWFnZXMvQXN0ZXJvaWQucG5nXCJdKTtcclxuICAgICAgICB0aGlzLmltYWdlMyA9IG5ldyBTcHJpdGUoY2FjaGVbXCJhc3NldHMvaW1hZ2VzL0NvaW4ucG5nXCJdKTtcclxuICAgICAgICB0aGlzLmltYWdlNCA9IG5ldyBTcHJpdGUoY2FjaGVbXCJhc3NldHMvaW1hZ2VzL0NvaW4ucG5nXCJdKTtcclxuICAgICAgICB0aGlzLmltYWdlNC54ID0gMjAwO1xyXG4gICAgICAgIHRoaXMuaW1hZ2U0LnkgPSA1MDtcclxuICAgICAgICB0aGlzLmltYWdlNC5hbHBoYSA9IDAuMTtcclxuICAgICAgICB0aGlzLmltYWdlNiA9IG5ldyBTcHJpdGUoY2FjaGVbXCJhc3NldHMvaW1hZ2VzL1NtYWxsUGFjLnBuZ1wiXSk7XHJcbiAgICAgICAgdGhpcy5hZGRDaGlsZCh0aGlzLmltYWdlNik7XHJcbiAgICAgICAgdGhpcy5hZGRDaGlsZCh0aGlzLmltYWdlKTtcclxuICAgICAgICB0aGlzLmFkZENoaWxkKHRoaXMuaW1hZ2UyKTtcclxuICAgICAgICB0aGlzLmFkZENoaWxkKHRoaXMuaW1hZ2UzKTtcclxuICAgICAgICB0aGlzLmFkZENoaWxkKHRoaXMuaW1hZ2U0KTtcclxuICAgICAgICB0aGlzLmFkZENoaWxkKHRoaXMuaW1hZ2U2KTtcclxuXHJcbiAgICAgICAgXHJcbiAgICAgICAgLy93aW5kb3cuYWRkRXZlbnRMaXN0ZW5lcihcImtleWRvd25cIix0aGlzLm9uS2V5RG93bi5iaW5kKHRoaXMpKTtcclxuICAgICAgICAvL3dpbmRvdy5hZGRFdmVudExpc3RlbmVyKFwia2V5dXBcIix0aGlzLm9uS2V5VXAuYmluZCh0aGlzKSk7XHJcbiAgICAgICAgLy93aW5kb3cuYWRkRXZlbnRMaXN0ZW5lcihcImtleXByZXNzXCIsdGhpcy5vbktleVByZXNzLmJpbmQodGhpcykpO1xyXG5cclxuICAgICAgICB3aW5kb3cuYWRkRXZlbnRMaXN0ZW5lcihcIm1vdXNlZG93blwiLHRoaXMubW91c2VEb3duLmJpbmQodGhpcykpO1xyXG4gICAgICAgIHdpbmRvdy5hZGRFdmVudExpc3RlbmVyKFwibW91c2Vtb3ZlXCIsdGhpcy5tb3VzZU1vdmUuYmluZCh0aGlzKSk7XHJcblxyXG4gICAgICAgIFxyXG4gICAgICAgIHRoaXMudGlja2VyLmFkZCh0aGlzLnVwZGF0ZU9ianMsdGhpcyk7XHJcbiAgICB9XHJcblxyXG4gICAgbW91c2VEb3duKHope1xyXG4gICAgICAgIHRoaXMuaW1hZ2UzLnggPSB6Lng7XHJcbiAgICAgICAgdGhpcy5pbWFnZTMueSA9IHoueTtcclxuICAgIH1cclxuXHJcbiAgICBtb3VzZU1vdmUocCl7XHJcbiAgICAgICAgdGhpcy5pbWFnZTIueCA9IHAueDtcclxuICAgICAgICB0aGlzLmltYWdlMi55ID0gcC55O1xyXG4gICAgfVxyXG4gICAgXHJcbiAgICB1cGRhdGUoc3QpXHJcbiAgICB7XHJcbiAgICAgICAgY29uc29sZS5sb2coXCJVcGRhdGVcIik7XHJcbiAgICAgICAgdGhpcy5lbGFwc2VkVGltZSArPSB0aGlzLnRpY2tlci5lbGFwc2VkTVM7XHJcbiAgICB9XHJcblxyXG4gICAgbW92ZU9iaihvYmopXHJcbiAgICB7XHJcbiAgICAgICAgb2JqLnggKz0gKHRoaXMuZGVsdGFUaW1lKCkqMTAwKTtcclxuICAgICAgICBpZihvYmoueD4xMDAwKVxyXG4gICAgICAgIHtcclxuICAgICAgICAgICAgb2JqLng9MDtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbiAgICByb3RhdGVPYmoob2JqKVxyXG4gICAge1xyXG4gICAgICAgIG9iai5yb3RhdGlvbiArPSAodGhpcy5kZWx0YVRpbWUoKSowLjI4KTtcclxuICAgIH1cclxuICAgIHVwZGF0ZVRleHQob2JqKVxyXG4gICAge1xyXG4gICAgICAgIC8vdGltZSArPSB0aGlzLnRpY2tlci5lbGFwc2VkTVM7XHJcbiAgICAgICAgLy9vYmouVGV4dCA9IFwiVGltZSA9IFwiK1xyXG4gICAgfVxyXG4gICAgbW92ZVhPYmoob2JqKVxyXG4gICAge1xyXG4gICAgICAgIG9iai54ICs9IDIwMDtcclxuICAgICAgICBpZihvYmoueD4xMDAwKVxyXG4gICAgICAgIHtcclxuICAgICAgICAgICAgb2JqLng9MDtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbiAgICBtb3ZlT3BhY2l0eU9iaihvYmopXHJcbiAgICB7XHJcbiAgICAgICAgb2JqLnkgKz0gKHRoaXMuZGVsdGFUaW1lKCkqMTAwKTtcclxuICAgICAgICBvYmouYWxwaGEgKz0gKHRoaXMuZGVsdGFUaW1lKCkqMS4pO1xyXG4gICAgICAgIGlmKG9iai5hbHBoYSA+PSAxLilcclxuICAgICAgICB7XHJcbiAgICAgICAgICAgIG9iai5hbHBoYSA9IDAuMTtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYob2JqLnk+NTAwKVxyXG4gICAgICAgIHtcclxuICAgICAgICAgICAgb2JqLnk9NTA7IFxyXG4gICAgICAgIH1cclxuICAgIH1cclxuICAgIHVwZGF0ZU9ianMoKVxyXG4gICAge1xyXG4gICAgICAgIC8vdGhpcy51cGRhdGVUZXh0KHRoaXMuZGlzcGxheUVsYXApO1xyXG4gICAgICAgIHRoaXMudGltZSArPSB0aGlzLmRlbHRhVGltZSgpO1xyXG4gICAgICAgIHRoaXMuc2Vjb25kICs9IHRoaXMuZGVsdGFUaW1lKCk7XHJcbiAgICAgICAgY29uc29sZS5sb2coXCJEZWx0YSA9IFwiK3RoaXMuZGVsdGFUaW1lKCkpO1xyXG4gICAgICAgIHRoaXMubW92ZU9iaih0aGlzLmltYWdlKTtcclxuICAgICAgICB0aGlzLnJvdGF0ZU9iaih0aGlzLmltYWdlKTtcclxuICAgICAgICB0aGlzLm1vdmVPcGFjaXR5T2JqKHRoaXMuaW1hZ2U0KTtcclxuICAgICAgICBpZih0aGlzLnNlY29uZD49MS4pXHJcbiAgICAgICAge1xyXG4gICAgICAgIHRoaXMubW92ZVhPYmoodGhpcy5pbWFnZTYpO1xyXG4gICAgICAgIHZhciBjYWNoZSA9IHV0aWxzLlRleHR1cmVDYWNoZTtcclxuICAgICAgICB0aGlzLmltYWdlNiA9IG5ldyBTcHJpdGUoY2FjaGVbXCJhc3NldHMvaW1hZ2VzL0NvaW4ucG5nXCJdKTtcclxuICAgICAgICB0aGlzLnNlY29uZCA9IDA7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG4gICAgZGVsdGFUaW1lKClcclxuICAgIHtcclxuICAgICAgICByZXR1cm4gdGhpcy50aWNrZXIuZWxhcHNlZE1TLzEwMDA7XHJcbiAgICB9XHJcblxyXG59XHJcbmV4cG9ydCBkZWZhdWx0IFRpdGxlU2NyZWVuOyJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7QUFBQTtBQUNBO0FBQUE7QUFDQTs7O0FBQUE7QUFDQTs7Ozs7Ozs7Ozs7QUFDQTs7O0FBQ0E7QUFBQTtBQUNBO0FBREE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFuQ0E7QUFvQ0E7QUFDQTs7O0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTtBQUNBOzs7QUFFQTtBQUVBO0FBQ0E7QUFDQTs7O0FBRUE7QUFFQTtBQUNBO0FBRUE7QUFDQTtBQUNBOzs7QUFDQTtBQUVBO0FBQ0E7OztBQUNBO0FBRUE7QUFDQTtBQUNBOzs7QUFDQTtBQUVBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7OztBQUNBO0FBRUE7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTs7OztBQWpIQTtBQUNBO0FBbUhBIiwic291cmNlUm9vdCI6IiJ9\n//# sourceURL=webpack-internal:///./src/screen/TitleScreen.js\n");

/***/ }),

/***/ "Config":
/*!*****************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** external "{\"BUILD\":{\"GAME_TITLE\":\"GAME TITLE\",\"VERSION\":\"0.0.1\",\"WIDTH\":889,\"HEIGHT\":500,\"MINIFIED_EXTERNAL_JS\":false,\"MINIFIED_EXTERNAL_JS_PATH\":\"./src/external/minified\",\"UNMINIFIED_EXTERNAL_JS_PATH\":\"./src/external/unminified\",\"EXTERNAL_JS\":[\"soundjs.js\",\"preloadjs.js\"],\"ASSETPATH\":{\"load\":\"assets\",\"audio\":\"assets/audio\"}}}" ***!
  \*****************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = {\"BUILD\":{\"GAME_TITLE\":\"GAME TITLE\",\"VERSION\":\"0.0.1\",\"WIDTH\":889,\"HEIGHT\":500,\"MINIFIED_EXTERNAL_JS\":false,\"MINIFIED_EXTERNAL_JS_PATH\":\"./src/external/minified\",\"UNMINIFIED_EXTERNAL_JS_PATH\":\"./src/external/unminified\",\"EXTERNAL_JS\":[\"soundjs.js\",\"preloadjs.js\"],\"ASSETPATH\":{\"load\":\"assets\",\"audio\":\"assets/audio\"}}};//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiQ29uZmlnLmpzIiwic291cmNlcyI6WyJ3ZWJwYWNrOi8vL2V4dGVybmFsIFwie1xcXCJCVUlMRFxcXCI6e1xcXCJHQU1FX1RJVExFXFxcIjpcXFwiR0FNRSBUSVRMRVxcXCIsXFxcIlZFUlNJT05cXFwiOlxcXCIwLjAuMVxcXCIsXFxcIldJRFRIXFxcIjo4ODksXFxcIkhFSUdIVFxcXCI6NTAwLFxcXCJNSU5JRklFRF9FWFRFUk5BTF9KU1xcXCI6ZmFsc2UsXFxcIk1JTklGSUVEX0VYVEVSTkFMX0pTX1BBVEhcXFwiOlxcXCIuL3NyYy9leHRlcm5hbC9taW5pZmllZFxcXCIsXFxcIlVOTUlOSUZJRURfRVhURVJOQUxfSlNfUEFUSFxcXCI6XFxcIi4vc3JjL2V4dGVybmFsL3VubWluaWZpZWRcXFwiLFxcXCJFWFRFUk5BTF9KU1xcXCI6W1xcXCJzb3VuZGpzLmpzXFxcIixcXFwicHJlbG9hZGpzLmpzXFxcIl0sXFxcIkFTU0VUUEFUSFxcXCI6e1xcXCJsb2FkXFxcIjpcXFwiYXNzZXRzXFxcIixcXFwiYXVkaW9cXFwiOlxcXCJhc3NldHMvYXVkaW9cXFwifX19XCI/ZDE0YiJdLCJzb3VyY2VzQ29udGVudCI6WyJtb2R1bGUuZXhwb3J0cyA9IHtcIkJVSUxEXCI6e1wiR0FNRV9USVRMRVwiOlwiR0FNRSBUSVRMRVwiLFwiVkVSU0lPTlwiOlwiMC4wLjFcIixcIldJRFRIXCI6ODg5LFwiSEVJR0hUXCI6NTAwLFwiTUlOSUZJRURfRVhURVJOQUxfSlNcIjpmYWxzZSxcIk1JTklGSUVEX0VYVEVSTkFMX0pTX1BBVEhcIjpcIi4vc3JjL2V4dGVybmFsL21pbmlmaWVkXCIsXCJVTk1JTklGSUVEX0VYVEVSTkFMX0pTX1BBVEhcIjpcIi4vc3JjL2V4dGVybmFsL3VubWluaWZpZWRcIixcIkVYVEVSTkFMX0pTXCI6W1wic291bmRqcy5qc1wiLFwicHJlbG9hZGpzLmpzXCJdLFwiQVNTRVRQQVRIXCI6e1wibG9hZFwiOlwiYXNzZXRzXCIsXCJhdWRpb1wiOlwiYXNzZXRzL2F1ZGlvXCJ9fX07Il0sIm1hcHBpbmdzIjoiQUFBQSIsInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///Config\n");

/***/ })

/******/ });